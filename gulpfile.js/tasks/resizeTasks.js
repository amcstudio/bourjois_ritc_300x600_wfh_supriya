var gulp = require('gulp'),
confirm = require('gulp-confirm'),
fs = require('fs'),
path = require('path'),
dom = require('gulp-dom'),
json,datajson,
bannerWidth = 300,
bannerHeight=250,
filePath = path.basename(path.dirname(path.dirname(__dirname)));


module.exports = {
    getBannerWidth : function (){
    json = JSON.parse(fs.readFileSync('./package.json'))
    datajson = JSON.parse(fs.readFileSync('./data.json'))

    return gulp.src('./src/')
        .pipe(confirm({
        question: 'Banner width? ',
        proceed: function (answer) {
            if (answer) {
            bannerWidth = answer;
            } else {
            bannerWidth = 300;
            }
            return true;
        }
        }))
    },

    getBannerHeight : function (){
        json = JSON.parse(fs.readFileSync('./package.json'))
        datajson = JSON.parse(fs.readFileSync('./data.json'))
        return gulp.src('./src/')
          .pipe(confirm({
            question: 'Banner height? ',
            proceed: function (answer) {
              if (answer) {
                bannerHeight = answer;
              } else {
                bannerHeight = 250;
              }
    
              return true;
            }
          }))
    },

    resizeInSass : function (){
      console.log(bannerWidth);
     fs.writeFile('./src/scss/_settings.scss', '', function (err) { })
     fs.appendFile('./src/scss/_settings.scss', '\n' + '$width:' + bannerWidth + 'px;' + '\n' + '$height:' + bannerHeight + 'px;', function (err) { });
    },

    insertDomResize : function (){
        return gulp.src('./src/index.html')
        .pipe(dom(function () {
  
  
          if (filePath) {
            this.getElementsByTagName('title')[0].innerHTML = filePath;
          }
          if (bannerWidth != undefined && bannerHeight != undefined) {
            this.getElementsByName('ad.size')[0].setAttribute('content', "width=" + bannerWidth + "\,height=" + bannerHeight);
          }
          return this;
        }))
        .pipe(gulp.dest('./src/'));
    }
}