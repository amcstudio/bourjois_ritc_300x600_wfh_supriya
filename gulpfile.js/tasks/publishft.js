var gulp = require('gulp'), 
    fs = require('fs'),
    zip = require('gulp-zip'),
    path = require('path'),
    filePath = path.basename(path.dirname(path.dirname(__dirname))),
    json;




    function publishRich() {
        return gulp.src(['./deploy/**/*'], {
          base: './deploy'
        })
    
          .pipe(zip(filePath.replace('.', '_') + "_rich.zip"))
          .pipe(gulp.dest('./_publishZip'));
      }


    function publishBase() {
    return gulp.src(['./ft_polite/**/*'], {
        base: './ft_polite'
    })

        .pipe(zip(filePath.replace('.', '_') + "_base.zip"))
        .pipe(gulp.dest('./_publishZip'));
    }
    
    
    module.exports = function (done) {
        publishRich();
        publishBase()
        done();
    }