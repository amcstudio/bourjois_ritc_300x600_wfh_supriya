var gulp = require('gulp'),
plumber = require('gulp-plumber'),
concat = require('gulp-concat'),
uglify = require('gulp-uglify'),
fs = require('fs'),
notify = require('gulp-notify');
datajson=JSON.parse(fs.readFileSync('./data.json')),
serverPath =datajson.wfh.serverPath,
remoteWork =datajson.wfh.remoteWork;

module.exports = {
    concat :function(){
        var alljsFiles = ['src/js/vendor/*',
        'src/js/olsTween*.js', 'src/js/adservers.js',
        '!src/js/politeLoadAnimation.js'
      ],
        animationjsFile = 'src/js/animation.js';
  
      if (fs.existsSync(animationjsFile)) {
        alljsFiles.push(animationjsFile);
      }
      if(remoteWork === 'y'){
        return gulp.src(alljsFiles)
        .pipe(plumber())
        .pipe(concat('main.js'))
       // .pipe(uglify())
        .pipe(gulp.dest('deploy/js'))
        .pipe(gulp.dest(serverPath+'/remoteMirror/js'))
        .pipe(notify("Scripts task complete"));
      }
      if(remoteWork === 'n'){
        return gulp.src(alljsFiles)
        .pipe(plumber())
        .pipe(concat('main.js'))
        .pipe(uglify())
        .pipe(gulp.dest('deploy/js'))
        .pipe(notify("Scripts task complete"));
      }
   
        
    },

    movePolite : function (){
      if(remoteWork === 'y'){
        return gulp.src(
          './src/js/politeLoadAnimation.js', { allowEmpty:true}
      )
      .pipe(plumber())
      .pipe(uglify())
      .pipe(gulp.dest('deploy/js'))
      .pipe(gulp.dest(serverPath+'/remoteMirror/js'))
      .pipe(notify("Scripts task complete"));
      }
      else{
        return gulp.src(
          './src/js/politeLoadAnimation.js', { allowEmpty:true}
      )
      .pipe(plumber())
      .pipe(uglify())
      .pipe(gulp.dest('deploy/js'))
      .pipe(notify("Scripts task complete"));
      }
        
    }
}