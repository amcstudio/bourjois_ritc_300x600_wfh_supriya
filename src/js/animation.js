'use strict';

~(function() {
    var $ = gsap,
        ad = document.getElementById('mainContent'),
        bgExit = document.getElementById('bgExit'),
        currentLoop = 1,
        loops = 3;

    window.init = function() {
        ad.style.display = 'block';
        bgExit.addEventListener('click', bgExitHandler);
        play();
    }

    function play(){
        var tl = gsap.timeline();

        tl.addLabel('frameOne')
        tl.to('#stripes',{ duration:0.7, x:-148, y:385, ease: 'Sine.easeOut' },'frameOne');
        tl.to('.imgSprite', { duration:0.7, x: 0, ease: Sine.easeOut },'frameOne+=0.5')

        tl.addLabel('frameTwo','+=1.5')
        tl.to('#stripes',{ duration:0.7,x:-500, y:369, backgroundColor:'#C9270C',ease: 'Sine.easeOut' },'frameTwo');
        tl.to('.imgSprite', { duration:0.7, x: -232, ease: Sine.easeOut },'frameTwo')
        tl.to('#cta', { duration:0.7,  backgroundColor:'#C9270C', ease: Sine.easeOut },'frameTwo')
        tl.to(['#productOne','.topcta','#topctaOne'], { duration:0.5, opacity:1, ease: Sine.easeOut },'frameTwo')

        tl.addLabel('frameThree','+=2')
        tl.to('#topctaOne', { duration:0.5, opacity:0, ease:'Sine.easeOut'},'frameThree')
        tl.to('#stripes', { duration:0.7,x:-900, backgroundColor:'#F52C4F',ease: 'Sine.easeOut' },'frameThree');
        tl.to('.imgSprite', { duration:0.7, x: -467, ease: Sine.easeOut },'frameThree')
        tl.to('#cta', { duration:0.7,  backgroundColor:'#F52C4F', ease: Sine.easeOut },'frameThree')
        tl.to(['#productTwo','#topctaTwo'], { duration:0.5, opacity:1, ease: Sine.easeOut },'frameThree')

        tl.addLabel('frameFour','+=2')
        tl.to('#topctaTwo', { duration:0.5, opacity:0, ease:'Sine.easeOut'},'frameFour')
        tl.to('#stripes',{ duration:0.7,x:-1200, backgroundColor:'#CF2B30',ease: 'Sine.easeOut' },'frameFour');
        tl.to('.imgSprite', { duration:0.7, x: -695, ease: Sine.easeOut },'frameFour')
        tl.to('#cta', { duration:0.7,  backgroundColor:'#CF2B30', ease: Sine.easeOut },'frameFour')
        tl.to(['#productThree','#topctaThree'], { duration:0.5, opacity:1, ease: Sine.easeOut },'frameFour')

        tl.addLabel('frameFive','+=2')
        tl.to('#topctaThree', { duration:0.5, opacity:0, ease:'Sine.easeOut'},'frameFive')
        tl.to('#stripes',{ duration:0.7,x:-1500, backgroundColor:'#C42942',ease: 'Sine.easeOut' },'frameFive');
        tl.to('.imgSprite', { duration:0.7, x: -933, ease: Sine.easeOut },'frameFive')
        tl.to('#cta', { duration:0.7,  backgroundColor:'#C42942', ease: Sine.easeOut },'frameFive')
        tl.to(['#productFour','#topctaFour'], { duration:0.5, opacity:1, ease: Sine.easeOut },'frameFive')

        tl.addLabel('frameSix','+=2')
        tl.to('#topctaFour', { duration:0.5, opacity:0, ease:'Sine.easeOut'},'frameSix')
        tl.to('#stripes',{ duration:0.7,x:-2000, backgroundColor:'#BB402A',ease: 'Sine.easeOut' },'frameSix');
        tl.to('.imgSprite', { duration:0.7, x: -1163, ease: Sine.easeOut },'frameSix')
        tl.to('#cta', { duration:0.7,  backgroundColor:'#BB402A', ease: Sine.easeOut },'frameSix')
        tl.to(['#productFive','#topctaFive'], { duration:0.5, opacity:1, ease: Sine.easeOut },'frameSix')

        tl.addLabel('frameSeven','+=1')
        tl.to(['.products','.topcta'], { duration:0.5, opacity:0, ease: Sine.easeOut },'frameSeven')
        tl.to('#stripes',{ duration:0.7,x:-2360, ease: 'Sine.easeOut' },'frameSeven');
        tl.to('.imgSprite', { duration:0.7, x: -1394, ease: Sine.easeOut },'frameSeven')
        tl.to('#imageSeven', { duration:1, opacity:1, x: 0, ease: Sine.easeOut },'frameSeven')
        tl.to('.img', { duration:0.5, opacity:0, ease: 'Sine.easeOut'},'frameSeven')
    }
        
       
       


    function bgExitHandler(e) {
        e.preventDefault();
        window.open(window.clickTag);
    }
})();